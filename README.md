# Exercise 1.7 Rotate Matrix

Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the image by 90 degrees. Can you do this in place?

_Hints:#51, #100_

This solution uses recursion to move the cells from the outermost level to the innermost level greather than 1 (matrix size).
