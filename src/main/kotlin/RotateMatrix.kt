class Matrix<T>(private val matrix: Array<Array<T>>) {
    private val numberOfRows = matrix.size
    private val numberOfCols = if (numberOfRows > 0) matrix[0].size else 0
    val size = if (numberOfCols == numberOfRows) numberOfCols else -1

    init {
        // Ensure the matrix is regular, MxN, i.e. all the rows have the same size.
        for (i in 0 until numberOfRows) require(matrix[i].size == numberOfCols)
    }

    fun isEmpty() = numberOfRows == 0
    fun isSquare() = numberOfCols == numberOfRows
    fun get(row: Int, col: Int): T = matrix[row][col]
    fun set(row: Int, col: Int, value: T) {
        matrix[row][col] = value
    }
}

fun rotateMatrix(matrix: Matrix<Int>) {
    tailrec fun rotateMatrix(layer: Int) {
        val size = matrix.size - 2 * layer
        tailrec fun rotateCells(offset: Int) {
            fun moveFourCells() {
                val tmp = matrix.get(layer, layer + offset)
                matrix.set(layer, layer + offset, matrix.get(layer + size - 1 - offset, layer))
                matrix.set(layer + size -1 - offset , layer, matrix.get(layer + size - 1, layer + size - 1 - offset))
                matrix.set(layer + size - 1, layer + size - 1 - offset, matrix.get(layer + offset, layer + size - 1))
                matrix.set(layer + offset, layer + size - 1, tmp)
            }

            if (offset >= size - 1) return
            moveFourCells()
            rotateCells(offset + 1)
        }

        if (size < 2) return
        rotateCells(0)
        rotateMatrix(layer + 1)
    }

    // For a square matrix, recursively process each later from the outermost (0) to the innermost (matrix size).
    if (matrix.isSquare()) rotateMatrix(0) else throw IllegalArgumentException("Not a square matrix!")
}
