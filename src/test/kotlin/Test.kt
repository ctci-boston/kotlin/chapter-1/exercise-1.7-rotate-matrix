import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Test {

    private fun runRotateTest(size: Int) {
        fun getTestMatrix(m: Int): Array<Array<Int>> = Array(m) { Array(m) { 0 } }
        val matrix: Matrix<Int> = Matrix(getTestMatrix(size))
        for (i in 0 until matrix.size) for (j in 0 until matrix.size) matrix.set(i, j, (i * matrix.size) + j + 1)
        rotateMatrix(matrix)
        assertFalse(matrix.isEmpty())
        for (i in 0 until matrix.size) for (j in 0 until matrix.size)
            assertEquals(((matrix.size * matrix.size) - (matrix.size - (i + 1)) - (j * matrix.size)), matrix.get(i, j))
    }

    @Test fun `Given an empty matrix verify no changes`() {
        fun intMatrix(m: Int): Array<Array<Int>> = Array(m) { Array(m) { 0 } }
        val matrix: Matrix<Int> = Matrix(intMatrix(0))
        rotateMatrix(matrix)
        assertTrue(matrix.isEmpty())
    }

    @Test fun `Verify matrix rotation on matrices of size 0 to 10`() {
        for (i in 1 until 10) runRotateTest(i)
    }
}
